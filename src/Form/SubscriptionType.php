<?php

namespace App\Form;

use App\Entity\Subscription;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SubscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('paiementFrequency', ChoiceType::class, [
                'choices' => [
                    '30 days' => 30,
                    '45 days' => 45,
                ],
            ])
            ->add('engagementDelay', ChoiceType::class, [
                'choices' => [
                    '1 month' => 1,
                    '3 months' => 3,
                    '6 months' => 6,
                ],
            ])
            ->add('price')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Subscription::class,
        ]);
    }
}
