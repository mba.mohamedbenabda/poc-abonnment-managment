<?php
namespace App\Command;

use App\Repository\UserRepository;
use App\Service\UserManager;
use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitiatePaymentCommand extends Command
{
    protected static $defaultName = 'app:initiate-payment';

    private $userRepository;

    private $userManager;

    public function __construct(UserRepository $userRepository, UserManager $userManager)
    {
        $this->userRepository = $userRepository;
        $this->userManager = $userManager;

        parent::__construct();
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {   
        $today = new DateTime('now');
        //TODO: to Update , get not adin users
        $users = $this->userRepository->findAll();

        foreach($users as $user) {
            if($this->userManager->mustPaySubscription($user, $today)) {
                $output->writeln(sprintf('Pay Subscription for User : %s', $user->getUsername()));
                $this->userManager->renewSubscription($user, $user->getLastSubscription(), $today);
            }
        }

        return Command::SUCCESS;
    }
}
?>