<?php

namespace App\Controller;

use App\Entity\Subscription;
use App\Form\SubscriptionType;
use App\Repository\SubscriptionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SubscriptionController extends AbstractController
{
    /**
     * @Route("/admin/subscriptions", name="app_admin_subscriptions_managment")
     */
    public function manageSubscriptions(SubscriptionRepository $subscriptionRepository): Response
    {
        $subscriptions = $subscriptionRepository->findAll();

        return $this->render('admin/subscription/list.html.twig', [
            'subscriptions' => $subscriptions,
        ]);
    }

    /**
     * @Route("/admin/subscription/creation", name="app_admin_subscription_creation", methods="GET|POST")
     *
     */
    public function createSubscription(Subscription $subscription = null, Request $request, EntityManagerInterface $manager): Response
    {   
        $subscription = new Subscription();
        $form = $this->createForm(SubscriptionType::class, $subscription);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($subscription);
            $manager->flush();
            $this->addFlash("success", "Subscription added successfully");

            return $this->redirectToRoute('app_admin_subscriptions_managment');
        }

        return $this->render('admin/subscription/creation.html.twig', [
            'form'    => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/subscription/{id}", name="app_admin_subscription_update", methods="GET|POST")
     * 
     */
    public function updateSubscription(Subscription $subscription, Request $request, EntityManagerInterface $manager): Response
    {   
        $form = $this->createForm(SubscriptionType::class, $subscription);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($subscription);
            $manager->flush();
            $this->addFlash("success", "Subscription updated successfully");

            return $this->redirectToRoute('app_admin_subscriptions_managment');
        }

        return $this->render('admin/subscription/creation.html.twig', [
            'form'    => $form->createView(),
        ]);
    }
}
