<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Transaction;
use App\Entity\Subscription;
use App\Form\UserBankingInfosType;
use App\Repository\UserRepository;
use App\Repository\TransactionRepository;
use App\Repository\SubscriptionRepository;
use App\Service\TransactionManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="app_user")
     */
    public function index(UserRepository $userRepository): Response
    {
        // $user = $userRepository->findOneBy(['username' => 'mba']);
        $user = $userRepository->findOneBy(['username' => 'arthur']);

        return $this->render('user/user_profile.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/user/{id}/chooseSubscription", name="app_user_choose_subscription")
     */
    public function chooseSubscription(User $user, SubscriptionRepository $subscriptionRepository): Response
    {
        $subscriptions = $subscriptionRepository->findAll();

        return $this->render('user/choose_subscription.html.twig', [
            'subscriptions' => $subscriptions,
            'user' => $user
        ]);
    }

    /**
     * @Route("/user/{id}/{subscriptionId}/chooseSubscription", name="app_user_affect_subscription")
     */
    public function affectSubscriptionToUser(User $user, Subscription $subscription, EntityManagerInterface $manager): Response
    {
        //todo :: add check mustPaySubscription from userManager

        if($user->hasValidBankingInformations())
        {
            $transaction = (new Transaction())
                ->setUser($user)
                ->setSubscription($subscription)
                ->setDate((new DateTime('now')))
                ->setAmount($subscription->getPrice())
                ;

            $manager->persist($transaction);

            $user->setLastPaymentDate((new DateTime('now')));
            $user->setLastRenewalDate((new DateTime('now')));
            $manager->persist($user);
            
            $manager->flush();

            $this->addFlash('success', sprintf('you are subscribed to the %s subscription', $subscription->getName()));

            return $this->redirectToRoute('app_user');
            
        } else {

            $this->addFlash('warning', 'please enter your credit card information');

            return $this->redirectToRoute('app_user_modify_banking_infos', [
                'id' => $user->getId()
            ]);
        }
    }

    /**
     * @Route("/user/{id}", name="app_user_modify_banking_infos")
     */
    public function modifyBankingInformations(User $user, EntityManagerInterface $manager, Request $request): Response
    {
        $form = $this->createForm(UserBankingInfosType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            
            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success', sprintf('modification of banking information is successfully completed for %s', $user->getUsername()));

            return $this->redirectToRoute('app_user');
        }

        return $this->render('user/modifyBankingInfos.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/users", name="app_admin_users_list", methods="GET|POST")
     * 
     */
    public function listUsers(UserRepository $userRepository): Response
    {   
        $users = $userRepository->findAll();

        return $this->render('admin/user/list.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/admin/user/{id}/purchases", name="app_admin_user_purchases", methods="GET|POST")
     * 
     */
    public function showPurchases(User $user): Response
    {   
        return $this->render('admin/user/list_transactions.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/admin/calculate/turnover", name="app_admin_turnover", methods="GET|POST")
     * @Route("/admin/calculate/turnover/{startDate}/{endDate}", name="app_admin_calculate_turnover", methods="GET|POST")
     * 
     */
    public function calculateTurnover(TransactionManager $transactionManager, string $startDate = null, string $endDate = null, Request $request): Response
    {   
        $startDate = date('Y:m:d', strtotime($request->get('start_date')));
        $endDate = date('Y:m:d', strtotime($request->get('end_date')));
        $showForm = true;

        if($request->get('search') !== null)
        {
            $showForm = false;
        }

        $result = $transactionManager->getTurnoverForPeriod($startDate, $endDate);

        return $this->render('admin/transaction/turnover.html.twig', [
            'transactions' => $result['transactions'],
            'sum' => $result['sum'],
            'showForm' => $showForm
        ]);
    }
}
