<?php
namespace App\Service;

use App\Repository\TransactionRepository;
use Psr\Log\LoggerInterface;

class TransactionManager
{
    private $logger;

    private $transactionRepository;

    public function __construct(LoggerInterface $logger, TransactionRepository $transactionRepository)
    {
        $this->logger = $logger;
        $this->transactionRepository = $transactionRepository;
    }

    public function getTurnoverForPeriod(string $startDate, string $endDate): array
    {
        $this->logger->info(sprintf('Start calculate Turnover from %s to %s', $startDate, $endDate));
        $sum = 0;
        $transactions = $this->transactionRepository->findForPeriod($startDate, $endDate);

        if(($startDate !== null) && ($endDate !== null)) {
            array_map(function ($transaction) use (&$sum) {
                $sum += $transaction->getAmount();
            }, $transactions);
        }

        $this->logger->info(sprintf('the total of the transactions is %s euros', $sum));

        return [
            'sum' => floatval($sum),
            'transactions' => $transactions
        ];
    }
}

?>
