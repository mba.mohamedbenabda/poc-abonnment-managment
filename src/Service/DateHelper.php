<?php
namespace App\Service;

use DateTime;
use Psr\Log\LoggerInterface;

class DateHelper
{
    private $logger;


    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function getIntervalInDays(DateTime $date, DateTime $today): string
    {
        $interval = $date->diff($today);
        
        return $interval->format('%R%a');
    }

    public function getIntervalInMonths(DateTime $date, DateTime $today): string
    {
        $interval = $date->diff($today);

        return $interval->format('%m');
    }
}

?>
