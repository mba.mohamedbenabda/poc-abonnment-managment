<?php
namespace App\Service;

use App\Entity\Subscription;
use App\Entity\Transaction;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\DateHelper;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\VarDumper\VarDumper;

class UserManager
{
    private $logger;

    private $userRepository;

    private $dateHelper;

    private $entityManager;

    public function __construct(LoggerInterface $logger, UserRepository $userRepository, DateHelper $dateHelper, EntityManagerInterface $manager)
    {
        $this->logger = $logger;
        $this->userRepository = $userRepository;
        $this->dateHelper = $dateHelper;
        $this->entityManager = $manager;
    }

    public function mustPaySubscription(User $user, DateTime $today): bool
    {
        $daysInterval = $this->dateHelper->getIntervalInDays($user->getLastPaymentDate(), $today);
        $mustPay = $user->getLastSubscription()->getPaiementFrequency() <= intval($daysInterval);

        return $mustPay;
    }

    public function mustRenewSubscription(User $user, DateTime $today): bool
    {
        $daysInterval = $this->dateHelper->getIntervalInMonths($user->getLastRenewalDate(), $today);
        $mustRenew = $user->getLastSubscription()->getEngagementDelay() <= intval($daysInterval);
        
        var_dump($daysInterval, $user->getLastSubscription()->getEngagementDelay(), $mustRenew);
        return $mustRenew;
    }

    public function renewSubscription(User $user, Subscription $subscription, DateTime $today) : void
    {
            $transaction = (new Transaction())
                ->setUser($user)
                ->setSubscription($subscription)
                ->setDate($today)
                ->setAmount($subscription->getPrice())
                ;

            $this->entityManager->persist($transaction);

            $user->setLastPaymentDate($today);

            if($this->mustRenewSubscription($user, $today)) {
                
                $newRenewalDate = $this->getNewRenewalDate($user->getLastRenewalDate(), $subscription);
                $user->setLastRenewalDate($newRenewalDate);
            }
            
            $this->entityManager->persist($user);
            $this->entityManager->flush();
    }

    public function getNewRenewalDate(DateTime $lastRenewalDate, Subscription $subscription): DateTime
    {
        $newRenewalDate = clone $lastRenewalDate;
        $interval = sprintf("P%sM", $subscription->getEngagementDelay());
        $newRenewalDate->add(new DateInterval($interval));

        return $newRenewalDate;
    }
}

?>
