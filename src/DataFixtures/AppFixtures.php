<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Subscription;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $user1 = (new User())
            ->setUsername('mba')
            ->setRoles(['ADMIN','USER'])
        ;
        $manager->persist($user1);

        $user2 = (new User())
            ->setUsername('arthur')
            ->setRoles(['USER'])
        ;
        $manager->persist($user2);

        $subscription1 = (new Subscription())
            ->setName('gold')
            ->setPaiementFrequency(30)
            ->setEngagementDelay(3)
            ->setPrice(10.0)
        ;
        $manager->persist($subscription1);

        $subscription2 = (new Subscription())
            ->setName('silver')
            ->setPaiementFrequency(45)
            ->setEngagementDelay(6)
            ->setPrice(7.5)
        ;
        $manager->persist($subscription2);

        $subscription3 = (new Subscription())
            ->setName('platinum')
            ->setPaiementFrequency(30)
            ->setEngagementDelay(1)
            ->setPrice(5.0)
        ;
        $manager->persist($subscription3);

        $manager->flush();
    }
}
